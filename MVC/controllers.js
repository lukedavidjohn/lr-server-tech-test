const { fetchRestaurants, fetchRestaurantById } = require("./models");

exports.getRestaurants = (req, res, next) => {
  const { vegan, dog, cuisines } = req.query;
  fetchRestaurants(vegan, dog, cuisines).then(restaurants => {
    res.status(200).send(restaurants);
  });
};

exports.getRestaurantById = (req, res, next) => {
  fetchRestaurantById(req.params)
    .then(restaurants => {
      if (restaurants.length === 0) {
        return Promise.reject({ status: 404, msg: "not found" });
      } else res.status(200).send(restaurants[0]);
    })
    .catch(next);
};
