const data = require("../restaurants.json");

exports.fetchRestaurants = async (vegan, dog, cuisines) => {
  if (!vegan && !dog && !cuisines) {
    return data;
  } else if (!vegan && !dog && cuisines) {
    const cuisineArray = cuisines.split(",").map(cuisine => {
      return cuisine.slice(0, 1).toUpperCase() + cuisine.slice(1);
    });
    console.log(cuisineArray);
  } else
    return data.filter(ele => {
      return (
        ele["vegan-options"] === JSON.parse(vegan) &&
        ele["dog-friendly"] === JSON.parse(dog)
      );
    });
};

exports.fetchRestaurantById = async ({ id }) => {
  return data.filter(ele => ele.id === Number(id));
};
