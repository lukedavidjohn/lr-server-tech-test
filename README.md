# Restaurant API Tech Test

Express REST API to serve data from JSON of restaurant information.

## Getting started

### Prerequisites

Node.js

### Setup

To test if Node.js is installed on your machine enter the following command in your command line:

```
npm -v
```

This should return a version number; if you still need to install Node.js please follow the instructions [here](https://nodejs.org/en/download/package-manager/).

Once you have Node.js set up on your machine, you will need to clone this repository to a local directory.

On the command line navigate to the parent directory where you would like to store the repository and run the following command:

```
git clone git clone https://lukedavidjohn@bitbucket.org/lukedavidjohn/lr-server-tech-test.git

```

Navigate into the directory on the command line then run the following command to install all of the dependencies from the package.json:

```
npm install
```

## Testing

This software was built with TDD using Mocha and Chai. The tests are contained in the spec folder. To run the tests, enter the following command in the command line:

```
npm test
```
