const { app } = require("../app");
const chai = require("chai");
const expect = chai.expect;
const request = require("supertest")(app);

describe("Express REST API", () => {
  describe("/restaurants", () => {
    it("serves array of all restaurants", () => {
      return request
        .get("/restaurants")
        .expect(200)
        .then(({ body }) => {
          expect(body.length).to.equal(6);
        });
    });
    it('takes optional "vegan-options" and "dog-friendly" parameters', () => {
      return request
        .get("/restaurants?vegan=true&dog=false")
        .expect(200)
        .then(({ body }) => {
          expect(body.length).to.equal(2);
        });
    });
    it('takes options "cuisines" parameter', () => {
      return request
        .get("/restaurants?cuisines=british,cafe")
        .expect(200)
        .then(({ body }) => {
          expect(body.length).to.equal(2);
        });
    });
  });
  describe("/restaurants/:id", () => {
    it("serves one restaurant-object matching provided id", () => {
      return request
        .get("/restaurants/1")
        .expect(200)
        .then(({ body }) => {
          expect(body.id).to.equal(1);
        });
    });
    it("returns 404 error if id not found", () => {
      return request.get("/restaurants/999").expect(404);
    });
  });
});
