const express = require("express");
const app = express();
const { router } = require("./router");

app.use("/restaurants", router);

module.exports = { app };
